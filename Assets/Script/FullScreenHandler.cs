﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class FullScreenHandler : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void registerFullScreenListeners();

    public Image iconRef;
    public Sprite Expand;
    public Sprite Shrink;

    bool isFullScreen;


    private void Awake()
    {
        if (this.name != "FullScreenHandler")
        {
            Debug.LogWarning("FullScreenHandler GameObject.name must be \"FullScreenHandler\"");
            this.name = "FullScreenHandler";
        }
        isFullScreen = Screen.fullScreen;
    }

    private void Start()
    {
        RegisterWebMobileOeientationListeners();
    }

    private void RegisterWebMobileOeientationListeners()
    {

        try
        {
            registerFullScreenListeners();
        }
        catch (Exception)
        {

            Debug.LogWarning("JS Error occured registerFullScreenListeners");
        } 

    }

    public void OnClick()
    {
        // Toggle fullscreen
        Screen.fullScreen = !Screen.fullScreen;
        Debug.Log("Toggle FullScreen");
    }
    private void Update()
    {
         if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            //codes for Landspace;
            Screen.fullScreen = !Screen.fullScreen;
        }

        if (isFullScreen != Screen.fullScreen)
        {
            UpdateIcon();
            isFullScreen = Screen.fullScreen;
        }
    }
    private void UpdateIcon()
    {
        iconRef.sprite = Screen.fullScreen ? Shrink : Expand;
    }
}

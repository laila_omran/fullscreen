﻿//copy when finish to jslib file

mergeInto(LibraryManager.library,
    {
        registerFullScreenListeners: function () { //send request to backEnd

            try {
                var unityInist;
                if ((!window.hasOwnProperty("unityInstance"))) {
                    console.error("Cant find unityInistance ");
                    if (window.hasOwnProperty("getUnityInstance")) {
                        unityInist = getUnityInstance();
                    } else {
                        console.error("Cant find getUnityInstance() ");
                        return;
                    }
                } else {
                    unityInist = window.unityInstance;
                }

                var OnOriantaion = function (ev) {
                    if (ev.target.screen.orientation.angle === 90 || ev.target.screen.orientation.angle === 270) {
                        unityInist.SetFullscreen(1);
                    }
                    else {
                        unityInist.SetFullscreen(0);
                    }
                }

                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    window.addEventListener('orientationchange', OnOriantaion);

                    var orientation = (screen.orientation || {}).type || screen.mozOrientation || screen.msOrientation;

                    if (orientation === "landscape-primary") {
                        //console.log("That looks good.");
                        unityInist.SetFullscreen(1);
                    } else if (orientation === "landscape-secondary") {
                        // console.log("Mmmh... the screen is upside down!");
                        unityInist.SetFullscreen(1);
                    } else if (orientation === "portrait-secondary" || orientation === "portrait-primary") {
                        // console.log("Mmmh... you should rotate your device to landscape");
                    } else if (orientation === undefined) {
                        // console.log("The orientation API isn't supported in this browser :(");
                    }


                } else {
                    console.error("Cant registerFullScreenListeners for non mobile browsers ");
                }
            } catch (e) {
                console.error(e);
            }
        }
    }
);